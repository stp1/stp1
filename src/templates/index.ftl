<div class="row placeholders">
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${filesCount}</div>
        <h4>Files</h4>
    </div>
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${linesCount}</div>
        <h4>Lines</h4>
    </div>
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${branchesCount}</div>
        <h4>Branches</h4>
    </div>
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${tagsCount}</div>
        <h4>Tags</h4>
    </div>
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${committersCount}</div>
        <h4>Committers</h4>
    </div>
    <div class="col-sm-2 col-xs-4 placeholder">
        <div class="text-count">${commitsCount}</div>
        <h4>Commits</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <h2 class="sub-header">Commits per Committer</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Committer</th>
                    <th>Percentage</th>
                </tr>
                </thead>
                <tbody>
                <#list committers as committer>
                <tr>
                    <td>${committer.getCommitterInfo()}</td>
                    <td>${((committer.getCommits()?size / commitsCount) * 100)?string.@decimal} %</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-4">
        <h2 class="sub-header">Commits per Branch</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Branch</th>
                    <th>Percentage</th>
                </tr>
                </thead>
                <tbody>
                <#list branches as branch>
                <tr>
                    <td>${branch.getTitle()}</td>
                    <td>${((branch.getCommits()?size / commitsCount) * 100)?string.@decimal} %</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-4">
        <h2 class="sub-header">Commits per Branch per Committer</h2>

    <#list branches as branch>
        <h4>Branch '${branch.getTitle()}'</h4>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Committer</th>
                    <th>Percentage</th>
                </tr>
                </thead>
                <tbody>
                    <#list committers as committer>
                    <tr>
                        <td>${committer.getCommitterInfo()}</td>
                        <td>${((branch.getCommitsCountByCommitter(committer) / branch.getCommits()?size) * 100)?string.@decimal}
                            %
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    </#list>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <h2 class="sub-header">Average Daily Commits per Committer</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Committer</th>
                    <th>Commits</th>
                </tr>
                </thead>
                <tbody>
                <#list committers as committer>
                <tr>
                    <td>${committer.getCommitterInfo()}</td>
                    <td>${committer.getAverageCommitsPerDay()}</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-4">
        <h2 class="sub-header">Average Weekly Commits per Committer</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Committer</th>
                    <th>Commits</th>
                </tr>
                </thead>
                <tbody>
                <#list committers as committer>
                <tr>
                    <td>${committer.getCommitterInfo()}</td>
                    <td>${committer.getAverageCommitsPerWeek()}</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-4">
        <h2 class="sub-header">Average Monthly Commits per Committer</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Committer</th>
                    <th>Commits</th>
                </tr>
                </thead>
                <tbody>
                <#list committers as committer>
                <tr>
                    <td>${committer.getCommitterInfo()}</td>
                    <td>${committer.getAverageCommitsPerMonth()}</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>

<h2 class="sub-header">Branches</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Branch</th>
            <th>Date Created</th>
            <th>Date Last Modified</th>
        </tr>
        </thead>
        <tbody>
        <#list branches as branch>
        <tr>
            <td>
                <a href="${branch.getPageFilename()}">
                ${branch.getTitle()} <i class="fa fa-chevron-right"></i>
                </a>
            </td>
            <td>${branch.formatDateCreated()}</td>
            <td>${branch.formatDateLastModified()}</td>
        </tr>
        </#list>
        </tbody>
    </table>
</div>