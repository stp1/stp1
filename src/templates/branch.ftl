<div class="row">
    <div class="col-sm-12">
        <a href="index.html" class="btn btn-primary">
            <i class="fa fa-chevron-left"></i> Back to Overview
        </a>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Message</th>
            <th>Date</th>
            <th>Committer</th>
            <th>Tags</th>
        </tr>
        </thead>
        <tbody>
        <#list commits as commit>
        <tr>
            <td>${commit.getID()}</td>
            <td>${commit.getMessage()}</td>
            <td>${commit.getDate()}</td>
            <td>${commit.getCommitterInfo()}</td>
            <td>Tags</td>
        </tr>
        </#list>
        </tbody>
    </table>
</div>