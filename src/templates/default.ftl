<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Git Repository Stats</title>

    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Git Repository Stats</a>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 main">
            <h1 class="page-header">${pageTitle}</h1>
        ${pageContent}
        </div>
    </div>
</div>

<script src="../assets/js/jquery-3.2.0.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>