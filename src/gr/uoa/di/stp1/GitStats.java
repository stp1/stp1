package gr.uoa.di.stp1;

import gr.uoa.di.stp1.dao.*;
import org.eclipse.jgit.api.Git;

import java.io.IOException;
import java.io.File;

public class GitStats {
    private Git mRepository;
    private String mPath;
    private Branches mBranches;
    private Commits mCommits;
    private Committers mCommitters;
    private Tags mTags;
    private Files mFiles;

    public GitStats(String path) {
        try {
            mRepository = Git.open(new File(path));
            mPath = path;
            mBranches = new Branches(mRepository);
            mCommits = new Commits(mRepository);
            mCommitters = new Committers(mRepository);
            mTags = new Tags(mRepository);
            mFiles = new Files(mRepository, mPath);
        } catch (IOException e) {
            System.out.println("Failed to initialize GitStats. Make sure that the provided repository exists.");
            System.exit(1);
        }
    }

    public Git getRepository() {
        return mRepository;
    }

    public Branches getBranches() {
        return mBranches;
    }

    public Commits getCommits() {
        return mCommits;
    }

    public Committers getCommitters() {
        return mCommitters;
    }

    public Tags getTags() {
        return mTags;
    }

    public Files getFiles() {
        return mFiles;
    }
}