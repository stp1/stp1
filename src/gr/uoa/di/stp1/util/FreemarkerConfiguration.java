package gr.uoa.di.stp1.util;

import freemarker.core.TemplateNumberFormatFactory;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import java.util.HashMap;
import java.util.Map;

public class FreemarkerConfiguration {
    private Configuration mConfig;

    public FreemarkerConfiguration(Configuration config) {
        mConfig = config;

        Map<String, TemplateNumberFormatFactory> customNumberFormats = new HashMap<>();
        customNumberFormats.put("decimal", DecimalTemplateNumberFormatFactory.INSTANCE);

        mConfig.setCustomNumberFormats(customNumberFormats);
        mConfig.setClassForTemplateLoading(this.getClass(), "/templates");
        mConfig.setDefaultEncoding("UTF-8");
        mConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        mConfig.setLogTemplateExceptions(false);
    }

    public Configuration getConfig() {
        return mConfig;
    }
}