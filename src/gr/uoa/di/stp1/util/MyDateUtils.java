package gr.uoa.di.stp1.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class MyDateUtils {
    public static String fromEpochSeconds(long seconds) {
        ZoneOffset offset = ZoneOffset.ofHours(2);
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(seconds, 0, offset);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm", Locale.ENGLISH);
        return dateTime.format(formatter);
    }
}
