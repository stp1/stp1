package gr.uoa.di.stp1.util;

import java.util.Locale;

import freemarker.core.*;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;

public class DecimalTemplateNumberFormatFactory extends TemplateNumberFormatFactory {
    public static final DecimalTemplateNumberFormatFactory INSTANCE = new DecimalTemplateNumberFormatFactory();

    private DecimalTemplateNumberFormatFactory() {
    }

    @Override
    public TemplateNumberFormat get(String params, Locale locale, Environment env) throws InvalidFormatParametersException {
        TemplateFormatUtil.checkHasNoParameters(params);
        return HexTemplateNumberFormat.INSTANCE;
    }

    private static class HexTemplateNumberFormat extends TemplateNumberFormat {

        private static final HexTemplateNumberFormat INSTANCE = new HexTemplateNumberFormat();

        private HexTemplateNumberFormat() {
        }

        @Override
        public String formatToPlainText(TemplateNumberModel numberModel) throws UnformattableValueException, TemplateModelException {
            Number n = TemplateFormatUtil.getNonNullNumber(numberModel);
            try {
                return String.format("%.2f", n.floatValue());
            } catch (ArithmeticException e) {
                throw new UnformattableValueException("Failed to format float");
            }
        }

        @Override
        public boolean isLocaleBound() {
            return false;
        }

        @Override
        public String getDescription() {
            return "Float with 2 decimal places";
        }
    }
}