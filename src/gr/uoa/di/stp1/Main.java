package gr.uoa.di.stp1;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Please specify local git repository path, and the path in which results will be exported.");
            System.out.println("The program will now exit.");
            System.exit(1);
        }

        String gitPath = args[0];
        String resultsPath = args[1];

        GitStats stats = new GitStats(gitPath);
        HtmlGenerator generator = new HtmlGenerator(resultsPath);
        generator.makeAll(stats);

        System.exit(0);
    }
}