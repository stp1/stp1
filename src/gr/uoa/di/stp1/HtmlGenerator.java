package gr.uoa.di.stp1;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import gr.uoa.di.stp1.dao.Branch;
import gr.uoa.di.stp1.util.FreemarkerConfiguration;
import gr.uoa.di.stp1.util.MyFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HtmlGenerator {
    private FreemarkerConfiguration mConfig;
    private String mExportPath;

    public HtmlGenerator(String exportPath) {
        Configuration config = new Configuration(Configuration.VERSION_2_3_25);
        mConfig = new FreemarkerConfiguration(config);
        mExportPath = exportPath;

        copyAssets();
    }

    private void copyAssets() {
        File destination = new File(mExportPath);

        try {
            if (destination.exists()) {
                FileUtils.deleteDirectory(destination);
            }

            FileUtils.forceMkdir(new File(mExportPath));
            FileUtils.forceMkdir(new File(mExportPath + "/assets"));
            FileUtils.forceMkdir(new File(mExportPath + "/html"));

            URL source = getClass().getClassLoader().getResource("assets");

            if (source.openConnection() instanceof JarURLConnection) {
                MyFileUtils.copyResourcesRecursively(source, new File(mExportPath + "/assets"));
            }
            else {
                MyFileUtils.copyResourcesRecursively(source, new File(mExportPath));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void make(String template, Map params, String fileName) {
        try {
            String filePath = mExportPath + "/html/" + fileName;

            File outFile = new File(filePath);

            if (outFile.createNewFile()) {
                System.out.println("Template file created successfully.");
            }
            else {
                System.out.println("Failed to create template file.");
            }

            Template tpl = mConfig.getConfig().getTemplate(template);
            StringWriter sw = new StringWriter();
            tpl.process(params, sw);

            Template rootTemplate = mConfig.getConfig().getTemplate("default.ftl");
            FileOutputStream stream = new FileOutputStream(outFile.getAbsolutePath());
            Writer out = new OutputStreamWriter(stream);
            params.put("pageContent", sw.toString());
            rootTemplate.process(params, out);

        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void makeIndex(GitStats stats) {
        Map<String, Object> params = new HashMap<>();
        params.put("pageTitle", "Overview");

        params.put("filesCount", stats.getFiles().count());
        params.put("linesCount", stats.getFiles().getTotalSourceLines());
        params.put("branchesCount", stats.getBranches().count());
        params.put("tagsCount", stats.getTags().count());
        params.put("committersCount", stats.getCommitters().count());
        params.put("commitsCount", stats.getCommits().count());

        params.put("repository", stats.getRepository());

        params.put("committers", stats.getCommitters().all());
        params.put("branches", stats.getBranches().all());

        make("index.ftl", params, "index.html");
    }

    public void makeBranches(GitStats stats) {
        List<Branch> branches = stats.getBranches().all();
        for (Branch branch : branches) {
            Map<String, Object> params = new HashMap<>();
            params.put("pageTitle", "Branch '" + branch.getTitle() + "'");
            params.put("commits", branch.getBranchCommits());
            make("branch.ftl", params, branch.getPageFilename());
        }
    }

    public void makeAll(GitStats stats) {
        makeIndex(stats);
        makeBranches(stats);
    }
}