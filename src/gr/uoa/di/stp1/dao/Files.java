package gr.uoa.di.stp1.dao;

import gr.uoa.di.stp1.util.MyFileUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Files {
    private List<String> mFiles;
    private String mPath;

    public Files(Git repository, String path) {
        File original = new File(path);
        String path2 = original.getAbsolutePath();
        File tmp = new File(path2);
        mPath = tmp.getParent();

        List<String> files = new ArrayList<>();

        Ref head = null;
        try {
            head = repository.getRepository().findRef("HEAD");

            try (RevWalk walk = new RevWalk(repository.getRepository())) {
                RevCommit commit = walk.parseCommit(head.getObjectId());
                RevTree tree = commit.getTree();

                try (TreeWalk treeWalk = new TreeWalk(repository.getRepository())) {
                    treeWalk.addTree(tree);
                    treeWalk.setRecursive(true);
                    while (treeWalk.next()) {
                        files.add(treeWalk.getPathString());
                    }

                    mFiles = files;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> all() {
        return mFiles;
    }

    public int count() {
        return mFiles.size();
    }

    public void printAll() {
        for (String file : mFiles) {
            System.out.println(file);
        }
    }

    public int getTotalSourceLines() {
        int lines = 0;

        for (String file : mFiles) {
            try {
                String extension = FilenameUtils.getExtension(file);
                if (!extension.equals("jar")) {
                    String absolutePath = mPath + "/" + file;
                    lines += MyFileUtils.lineCount(absolutePath);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return lines;
    }
}