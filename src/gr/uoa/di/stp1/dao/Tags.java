package gr.uoa.di.stp1.dao;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import java.util.ArrayList;
import java.util.List;

public class Tags {
    private List<Ref> mTags;
    private Git mRepository;

    public Tags(Git repository) {
        mTags = new ArrayList<>();
        mRepository = repository;

        List<Ref> call = null;
        try {
            call = repository.tagList().call();

            mTags.addAll(call);
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    public List<Ref> all() {
        return mTags;
    }

    public int count() {
        return mTags.size();
    }

    public void printAll() {
        for (Ref ref : mTags) {
            System.out.println("Tag: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
            getTagHead(ref);
        }
    }

    public RevCommit getTagHead(Ref ref) {
        RevCommit commit = null;

        try {
            LogCommand log = new Git(mRepository.getRepository()).log();

            Ref peeledRef = mRepository.getRepository().peel(ref);
            if (peeledRef.getPeeledObjectId() != null) {
                log.add(peeledRef.getPeeledObjectId());
            }
            else {
                log.add(ref.getObjectId());
            }

            Iterable<RevCommit> logs = log.call();
            for (RevCommit rev : logs) {
                commit = rev;
                break;
            }
        } catch (MissingObjectException e) {
            e.printStackTrace();
        } catch (IncorrectObjectTypeException e) {
            e.printStackTrace();
        } catch (NoHeadException e) {
            e.printStackTrace();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        return commit;
    }
}
