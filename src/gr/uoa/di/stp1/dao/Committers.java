package gr.uoa.di.stp1.dao;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.revwalk.RevCommit;

import java.util.ArrayList;
import java.util.List;

public class Committers {
    private List<Committer> mCommitters;
    private Git mRepository;

    public Committers(Git repository) {
        List<Committer> committers = new ArrayList<>();
        List<String> tmp = new ArrayList<>();

        Commits commits = new Commits(repository);
        for (RevCommit commit : commits.all()) {
            if (!tmp.contains(commit.getCommitterIdent().getEmailAddress())) {
                committers.add(new Committer(repository, commit.getCommitterIdent()));
                tmp.add(commit.getCommitterIdent().getEmailAddress());
            }
        }

        mCommitters = committers;
        mRepository = repository;
    }

    public List<Committer> all() {
        return mCommitters;
    }

    public int count() {
        return mCommitters.size();
    }
}