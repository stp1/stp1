package gr.uoa.di.stp1.dao;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;

import java.util.ArrayList;
import java.util.List;

public class Branches {
    private List<Branch> mBranches;
    private Git mRepository;

    public Branches(Git repository) {
        List<Branch> branches = new ArrayList<>();

        try {
            List<Ref> result = repository.branchList().call();

            for (Ref ref : result) {
                branches.add(new Branch(repository, ref));
            }
        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        mBranches = branches;
        mRepository = repository;
    }

    public List<Branch> all() {
        return mBranches;
    }

    public Git getRepository() {
        return mRepository;
    }

    public int count() {
        return mBranches.size();
    }
}