package gr.uoa.di.stp1.dao;

import gr.uoa.di.stp1.util.MyDateUtils;
import org.eclipse.jgit.revwalk.RevCommit;

public class Commit {
    private RevCommit mRevCommit;

    public Commit(RevCommit revCommit) {
        mRevCommit = revCommit;
    }

    public RevCommit getRevCommit() {
        return mRevCommit;
    }

    public String getID() {
        return mRevCommit.toObjectId().getName();
    }

    public String getMessage() {
        return mRevCommit.getFullMessage();
    }

    public String getDate() {
        return MyDateUtils.fromEpochSeconds(mRevCommit.getCommitTime());
    }

    public String getCommitterInfo() {
        return mRevCommit.getCommitterIdent().getName() + " (" + mRevCommit.getCommitterIdent().getEmailAddress() + ")";
    }
}