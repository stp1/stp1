package gr.uoa.di.stp1.dao;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.StopWalkException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class Committer {
    private PersonIdent mIdent;
    private Git mRepository;
    private List<RevCommit> mCommits;

    public Committer(Git repository, PersonIdent ident) {
        mRepository = repository;
        mIdent = ident;

        List<RevCommit> commitsList = new ArrayList<>();

        RevFilter filter = new RevFilter() {
            @Override
            public boolean include(RevWalk revWalk, RevCommit revCommit) throws StopWalkException, MissingObjectException, IncorrectObjectTypeException, IOException {
                return (revCommit.getCommitterIdent().getEmailAddress().equals(mIdent.getEmailAddress()));
            }

            @Override
            public RevFilter clone() {
                return null;
            }
        };

        try {
            Iterable<RevCommit> result = mRepository.log().all().setRevFilter(filter).call();

            for (RevCommit commit : result) {
                commitsList.add(commit);
            }
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        }

        mCommits = commitsList;
    }

    public PersonIdent getIdent() {
        return mIdent;
    }

    public Git getRepository() {
        return mRepository;
    }

    public List<RevCommit> getCommits() {
        return mCommits;
    }

    public String getAverageCommitsPerDay() {
        int start = mCommits.get(mCommits.size() - 1).getCommitTime();

        LocalDate startDate = Instant.ofEpochSecond(start).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = LocalDate.now();
        float days = Period.between(startDate, endDate).getDays();

        if (days == 0)
            return "Not enough data";

        return String.format("%.3f", mCommits.size() / days);
    }

    public String getAverageCommitsPerWeek() {
        int start = mCommits.get(mCommits.size() - 1).getCommitTime();

        LocalDate startDate = Instant.ofEpochSecond(start).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = LocalDate.now();
        float weeks = Period.between(startDate, endDate).getDays() / 7;

        if (weeks == 0)
            return "Not enough data";

        return String.format("%.3f", mCommits.size() / weeks);
    }

    public String getAverageCommitsPerMonth() {
        int start = mCommits.get(mCommits.size() - 1).getCommitTime();

        LocalDate startDate = Instant.ofEpochSecond(start).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = LocalDate.now();
        float months = Period.between(startDate, endDate).getMonths();

        if (months == 0)
            return "Not enough data";

        return String.format("%.3f", mCommits.size() / months);
    }

    public String getCommitterInfo() {
        return mIdent.getName() + " (" + mIdent.getEmailAddress() + ")";
    }
}