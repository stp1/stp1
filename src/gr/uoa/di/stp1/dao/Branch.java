package gr.uoa.di.stp1.dao;

import gr.uoa.di.stp1.util.MyDateUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Branch {
    private Git mRepository;
    private Ref mRef;
    private List<Commit> mCommits;
    private long mDateCreated;
    private long mDateLastModified;

    public Branch(Git repository, Ref ref) {
        mRepository = repository;
        mRef = ref;
        mCommits = getBranchCommits();
        mDateCreated = mCommits.get(mCommits.size() - 1).getRevCommit().getCommitTime();
        mDateLastModified = mCommits.get(0).getRevCommit().getCommitTime();
    }

    public Git getRepository() {
        return mRepository;
    }

    public Ref getRef() {
        return mRef;
    }

    public String getTitle() {
        String refName = mRef.getName();
        return refName.substring(refName.lastIndexOf('/') + 1);
    }

    public List<Commit> getCommits() {
        return mCommits;
    }

    public long getDateCreated() {
        return mDateCreated;
    }

    public long getDateLastModified() {
        return mDateLastModified;
    }

    public String formatDateCreated() {
        return MyDateUtils.fromEpochSeconds(mDateCreated);
    }

    public String formatDateLastModified() {
        return MyDateUtils.fromEpochSeconds(mDateLastModified);
    }

    public String getPageFilename() {
        return "branch_" + mRef.getObjectId().getName() + ".html";
    }

    public List<Commit> getBranchCommits() {
        List<Commit> commits = new ArrayList<>();

        try {
            Ref head = mRepository.getRepository().exactRef(mRef.getName());

            try (RevWalk walk = new RevWalk(mRepository.getRepository())) {
                RevCommit commit = walk.parseCommit(head.getObjectId());
                walk.markStart(commit);

                for (RevCommit rev : walk) {
                    commits.add(new Commit(rev));
                }
                walk.dispose();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return commits;
    }

    public int getCommitsCountByCommitter(Committer committer) {
        int count = 0;

        for (Commit commit : mCommits)
            if (commit.getRevCommit().getCommitterIdent().getEmailAddress().equals(committer.getIdent().getEmailAddress()))
                count++;

        return count;
    }
}