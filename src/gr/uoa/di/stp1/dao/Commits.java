package gr.uoa.di.stp1.dao;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Commits {
    private Git mRepository;
    private List<RevCommit> mCommits;

    public Commits(Git repository) {
        Iterable<RevCommit> result = null;
        List<RevCommit> commitsList = new ArrayList<>();

        try {
            result = repository.log().all().call();

            for (RevCommit commit : result) {
                commitsList.add(commit);
            }
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        }

        mRepository = repository;
        mCommits = commitsList;
    }

    public List<RevCommit> all() {
        return mCommits;
    }

    public int count() {
        return mCommits.size();
    }
}